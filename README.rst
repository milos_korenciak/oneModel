oneModel
========
Common (relational) data classes template / base for the whole app with:

* Optional (de)serialization capabilities (in-price).
* Modular lazy-computed data fields.
* Each class have aux - auxiliary data for flexibility. Stored as JSON. (Honored in PostgreSQL through HStore feature.) Strict relational model does not limit you!
* Async & parallel - using SqlAlchemy async features, but still easy usable from "sync" world.

Why?
====
Your app becomes bigger once... Then you want to break it down into several (Git) 
codebases. No matter if it is Desktop app (plugins), Server app (Micro-Services), 
embedded device or so. Once you undergo the split, the architecture matters to you.

The project canonizes the Model (from MVC = Model-View-Controller / MVP Model-View-Presenter)
using SQLAlchemy as base validator = (relational) data model with STRONG typing (check, enforcement).

Optionally it adds (de)serialization capabilities to your Model for popular formats: JSON,
raw Py dict, YAML, msgpack, ... (Write me a pull-request / issue to add your desired one.)
Anyway - you are NOT required to install / depend on any of the extra (de)serialization libraries.

For flexibility, you can store un-structured data in 'aux' (auxiliary) JSON field. (As the world often un-structured...)

Features supported:
-------------------

* Various serializations: JSON, (Py raw) dict, SQL, ...
* Flexible unstructured 'aux' (auxiliary) data in any class (if wanted).
* Caching. (Of the class structure. Do this to reload the class stucture.)
* Lazy initializations.
* Easy pure RAM work with data.
* Forward version migration (facility).

Optimization tip 1:
-------------------
Create more .py files if your data model does not need to be loadded in whole each time.
Python load machinery will load only \*.py files really needed by imports.

Optimization tip 2:
-------------------
Choose your desired DataBase / DataStore data backend wisely even for the first time. Usually, 
developers are used to not to change it later (despite SQLAlchemy makes it ridiculously easy 
for SQL DBs).

Thanks to
---------

* Marvelous SQLAlchemy team!
* Chat GPT-4!
* Solargis.com for inspiration.
* OpenSource community to check this repo out.

Currently an idea placeholder.

Intention is: basics of common data model for whole (GUI? headless?) app with all the features needed, but as much optional as possible.



