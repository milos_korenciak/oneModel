"""Example data model for tests beside.
Use as an example to 
"""

from oneModel import base as B
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import Mapped

class User(B.Base):
    name : Mapped[str] = Column(String)
    national_id = Column(String)
    age = Column(Integer)


class Address(B.Base):
    street = Column(String)