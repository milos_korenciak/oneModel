"""Test basic usage with SQLAlchemy"""


import unittest as U


engine = create_engine('postgresql://user:password@localhost/database')
Base.metadata.create_all(engine)

my_model = MyModel('John Doe', {'age': 30, 'country': 'USA'})
session.add(my_model)
session.commit()

my_model = session.query(MyModel).filter_by(name='John Doe').one()


data = my_model.data
age = data['age']
country = data['country']