

"""Created /w help of chat GPT-4."""
from abc import abstractmethod
from sqlalchemy import Column, JSON
from sqlalchemy.ext.declarative import DeclarativeBase
from sqlalchemy.orm import DeclarativeBase


# use the DeclarativeBase to create the usable Base itself
class Base(DeclarativeBase):
    """Subclass this Base class.
    All subclasses can define """
    __tablename__ = 'Base'
    yaml_tag = u'!Base'
    _members_cache = {}  # caches lists of direct & inherited memebers, use wisely

    # id = Column(Integer, primary_key=True)
    data = Column(JSON)

    @abstractmethod
    def __init__(self, *args, **kwargs):
        """Base stub to initialize the class.
        Marked as abstract to enforce its subclassing. Just calls super __init__."""
        super().__init__(*args, **kwargs)

    @classmethod
    def 
    @classmethod
    def get_

i